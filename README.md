# Base Theme
Prahran High School - Custom Wordpress Theme
Completed in June 2019, by Will Brooks

!-- This is a base theme only, some assets have been intentionally removed --!

Requirements:
- WordPress Version: 4.9.6 or higher
- PHP Version: 5.2.4 or higher

Technologies:
- HTML5, CSS3/SASS, PHP, MYSWL & jQuery

Libraries:
- ACF, Advanced Custom Fields (https://www.advancedcustomfields.com/)
- Scroll Reveal (https://scrollrevealjs.org/)
- Slick Slider (https://kenwheeler.github.io/slick/)
- iziModal (https://github.com/marcelodolza/iziModal)
- Styled GMaps (https://mapstyle.withgoogle.com/)
