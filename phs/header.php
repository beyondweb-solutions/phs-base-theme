<?php
/**
 * The header for our theme
 *
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>

<?php if( get_field('custom_css') ):
    $custom_css = get_field( 'custom_css' ); ?>
    <style type="text/css">
        <?php echo $custom_css;?>
    </style>
<?php endif; ?>
</head>

<body <?php body_class(); ?>>

<div id="page" class="site">

	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'phs' ); ?></a>

	<header id="masthead" class="site-header" role="banner" style="background: <?php the_field('header_bg_color', 'option') ?> url('<?php the_field('header_bg_image', 'option') ?>')">

    <div class="wrap">
			<div class="navigation-top">

          <div class="header-logo">
                  <a class="" href="<?php echo home_url(); ?>">
                      <img src="<?php the_field('desktop_logo', 'option') ?>" class="logo svg desktop">
                  </a>
          </div>

          <div class="mobile-menu-btn">
              <button data-izimodal-open="#menu-modal" id="menu-btn" class="hamburger hamburger--squeeze" type="button">
                    <span class="hamburger-box">
                      <span class="hamburger-inner"></span>
                    </span>
              </button>
          </div>
        </div>
			</div>

	</header><!-- #masthead -->


		<div id="content" class="site-content">
