/* Slider Code */


(function($) {

    var $intro_slider = $('.intro-slider');
    var $contact_slider = $('.contact-slider');
    var $ourschool_main_slider = $('.ourschool-mainslider');
    var $ourschool_secondary_slider = $('.ourschool-secondaryslider');

    $( document ).ready(function() {
        $intro_slider.slick({
            arrows: false,
            fade: false,
            rows: 0,
            speed: 750,
            autoplay: true,
            autoplaySpeed: 3500,
            infinite: true,
            swipe: true,
            swipeToSlide: true,
            touchThreshold: 10,
            responsive: [
                {
                    breakpoint: 9999,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 1280,
                    settings: {
                        dots: true,
                        slidesToShow: 3,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 1024,
                    settings: {
                        dots: true,
                        slidesToShow: 2,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        dots: true,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 420,
                    settings: {
                        dots: true,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                }
            ]
        });
        $intro_slider.show();

        function checkslideWidth(){
          $vWidth = $(window).width();
          //Check condition for screen width
          if($vWidth > 1200){

                function loop() {
                    $(".intro-slider .slick-track").each(function() {
                        var current = $(this).children(".slick-current").removeClass("slick-current");
                        var i = current.next().length ? current.index() : 0;
                        current.siblings(":eq(" + i + ")").addClass("slick-current");
                    });
                }
                var interval = setInterval(loop, 5000);

                $(".intro-slide").hover(function() {
                  $(".intro-slide").removeClass('slick-current');
                  $(this).addClass('slick-current');
                   clearInterval(interval);
                  }, function() {
                        $(".intro-slide").removeClass('slick-current');
                        $(this).addClass('slick-current');
                        interval = setInterval(loop, 5000);
                  });

              }
          }
          checkslideWidth();

          $contact_slider.slick({
              arrows: false,
              fade: false,
              rows: 0,
              speed: 750,
              autoplay: true,
              autoplaySpeed: 3500,
              infinite: true,
              swipe: true,
              swipeToSlide: true,
              touchThreshold: 10,
              responsive: [
                  {
                      breakpoint: 9999,
                      settings: {
                          slidesToShow: 4,
                      }
                  },
                  {
                      breakpoint: 1024,
                      settings: {
                          dots: true,
                          slidesToShow: 3,
                          slidesToScroll: 1,
                      }
                  },
                  {
                      breakpoint: 768,
                      settings: {
                          dots: true,
                          slidesToShow: 3,
                          slidesToScroll: 1,
                      }
                  },
                  {
                      breakpoint: 600,
                      settings: {
                          dots: true,
                          slidesToShow: 1,
                          slidesToScroll: 1,
                      }
                  }
              ]
          });
          $contact_slider.show();

          $ourschool_main_slider.slick({
              arrows: false,
              fade: false,
              dots: true,
              speed: 750,
              rows: 0,
              fade: true,
              autoplay: true,
              autoplaySpeed: 7500,
              infinite: true,
              swipe: true,
              swipeToSlide: true,
              touchThreshold: 10,
              slidesToShow: 1,
              slidesToScroll: 1,
              asNavFor: '.ourschool-secondaryslider'

          });
          $ourschool_main_slider.show();

          $ourschool_secondary_slider.slick({
              arrows: false,
              fade: false,
              dots: false,
              vertical: true,
              rows: 0,
              asNavFor: '.ourschool-mainslider',
              responsive: [
                  {
                      breakpoint: 9999,
                      settings: {
                          slidesToShow: 5,
                      }
                  }
              ]
          });
          $ourschool_secondary_slider.show();
          $('a[data-slide]').click(function(e) {
            e.preventDefault();
            var slideno = $(this).data('slide');
            $('.ourschool-mainslider').slick('slickGoTo', slideno - 1);
          });
    });



})(jQuery);
