(function($) {

  function autorun(){

       var rect = $('#front-hero-container')[0].getBoundingClientRect();
       var mouse = {x: 0, y: 0, moved: false};

       $("#front-hero-container").mousemove(function(e) {
         mouse.moved = true;
         mouse.x = e.clientX - rect.left;
         mouse.y = e.clientY - rect.top;
       });


       function explode(){
         $(".fade-in-btn").css({"visibility": "visible", "opacity": "1"});
       }
       setTimeout(explode, 2000);

       function explode1(){
       // Ticker event will be called on every frame
       TweenLite.ticker.addEventListener('tick', function(){
         if (mouse.moved){
           parallaxIt(".front-hero-image-one", -100);
           parallaxIt(".front-hero-image-two", 100);
           parallaxIt(".front-hero-image-three", 50);
           parallaxIt(".bg-image-main", -30);
         }
         mouse.moved = false;
       });

         function parallaxIt(target, movement) {
           TweenMax.to(target, 0.3, {
             x: (mouse.x - rect.width / 2) / rect.width * movement,
             y: (mouse.y - rect.height / 2) / rect.height * movement
           });
         }

         $(window).on('resize scroll', function(){
           rect = $('#front-hero-container')[0].getBoundingClientRect();
         })
       }
       setTimeout(explode1, 5000);

       function explode2(){
         $(".fade-in-bg").css({"visibility": "visible", "opacity": "1"});
       }
       setTimeout(explode2, 6000);

       function explode3(){
         $(".fade-in").css({"visibility": "visible", "opacity": "1"});
       }
       setTimeout(explode3, 7000);


  }
  if (window.addEventListener) window.addEventListener("load", autorun, false);
  else if (window.attachEvent) window.attachEvent("onload", autorun);
  else window.onload = autorun;


})(jQuery);
