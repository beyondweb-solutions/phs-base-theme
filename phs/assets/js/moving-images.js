(function($) {

  function autorun(){

       function lifePHSimages(){
         $(document).ready(function() {
         checkWidth();
         });
         $(window).on("resize", function(){
             checkWidth();
         });
       }

       function checkWidth(){
         $vWidth = $(window).width();
         //Check condition for screen width
         if($vWidth > 900){
           var recttwo = $('#phs-moving-image')[0].getBoundingClientRect();
           var mousetwo = {x: 0, y: 0, moved: false};
           
           $("#phs-moving-image").mousemove(function(e) {
             mousetwo.moved = true;
             console.log();
             mousetwo.x = e.clientX - recttwo.left;
             mousetwo.y = e.clientY - recttwo.top;
           });

           function explode4(){
           // Ticker event will be called on every frame
           TweenLite.ticker.addEventListener('tick', function(){
             if (mousetwo.moved){
               parallaxItTwo(".life-phs-image-main.image-1", -50);
               parallaxItTwo(".life-phs-image-main.image-2", 50);
               parallaxItTwo(".life-phs-image-main.image-3", 25);
             }
             mousetwo.moved = false;
           });

             function parallaxItTwo(target, movement) {
               TweenMax.to(target, 0.3, {
                 x: (mousetwo.x - recttwo.width / 2) / recttwo.width * movement,
                 y: (mousetwo.y - recttwo.height / 2) / recttwo.height * movement
               });
             }

             $(window).on('resize scroll', function(){
               recttwo = $('#phs-moving-image')[0].getBoundingClientRect();
             })
           }
           setTimeout(explode4, 1000);
             }
         }

         lifePHSimages();


  }
  if (window.addEventListener) window.addEventListener("load", autorun, false);
  else if (window.attachEvent) window.attachEvent("onload", autorun);
  else window.onload = autorun;


})(jQuery);
