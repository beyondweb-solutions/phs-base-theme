(function($) {

/*--------------------------------------------------------------
Scroll Distance
--------------------------------------------------------------*/

  $scrolledDistanceFromTop = $("#masthead").outerHeight();

  $(window).scroll(function() {
    var scrollPosition = $(window).scrollTop();

    if (scrollPosition >= $scrolledDistanceFromTop) {
      $("body").addClass("has-scrolled");
    } else {
      $("body").removeClass("has-scrolled");
    }
  });

/*--------------------------------------------------------------
On Load Animations
--------------------------------------------------------------*/

  $(window).load(function() {
      $('html').addClass('js-loaded');
      $('#register-modal').hide();
      $("#location-modal").hide();
      $("#video-modal").hide();
      setTimeout(function() {
          $("html").removeClass("is-transitioning");
      }, 250);

/*--------------------------------------------------------------
Scroll Animations
--------------------------------------------------------------*/

function scrollReveal() {
      ScrollReveal().reveal('.content-scroll', {
          delay: 200,
          duration: 500,
          distance: '30px',
          reset: false
      });
      ScrollReveal().reveal('.image-scroll', {
          delay: 200,
          duration: 500,
          reset: false,
          distance: '30px'
      });
  }
  scrollReveal();
});


/*--------------------------------------------------------------
Doco Ready
--------------------------------------------------------------*/
  $(document).ready(function() {


    function applyValidation(x) {
      var msg = "Please enter" + x.name + " before send";
      x.setCustomValidity(msg);
    }



     /*--------------------------------------------------------------
      Replace all SVG images with inline SVG
     --------------------------------------------------------------*/
    $(function() {
      $("img.svg").each(function() {
        var $img = $(this);
        var imgID = $img.attr("id");
        var imgClass = $img.attr("class");
        var imgURL = $img.attr("src");
        $.get(
          imgURL,
          function(data) {
            // Get the SVG tag, ignore the rest
            var $svg = $(data).find("svg");
            // Add replaced image's ID to the new SVG
            if (typeof imgID !== "undefined") {
              $svg = $svg.attr("id", imgID);
            }
            // Add replaced image's classes to the new SVG
            if (typeof imgClass !== "undefined") {
              $svg = $svg.attr("class", imgClass + " replaced-svg");
            }
            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr("xmlns:a");
            // Replace image with new SVG
            $img.replaceWith($svg);
          },
          "xml"
        );
      });
    });


    /*--------------------------------------------------------------
     Smooth Scroll
    --------------------------------------------------------------*/
    function SmoothScroll() {
              // Select all links with hashes
          $('a[href*="#"]')
            // Remove links that don't actually link to anything
            .not('[href="#"]')
            .not('[href="#0"]')
            .click(function(event) {
              // On-page links
              if (
                location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
                &&
                location.hostname == this.hostname
              ) {
                // Figure out element to scroll to
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                // Does a scroll target exist?
                if (target.length) {
                  // Only prevent default if animation is actually gonna happen
                  event.preventDefault();
                  $('html, body').animate({
                    scrollTop: target.offset().top - 80
                  }, 1000, function() {
                    // Callback after animation
                    // Must change focus!
                    var $target = $(target);
                    $target.focus();
                    if ($target.is(":focus")) { // Checking if the target was focused
                      return false;
                    } else {
                      $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                      $target.focus(); // Set focus again
                    };
                  });
                }
              }
            });
          }


    /*--------------------------------------------------------------
     Mobile Menu
    --------------------------------------------------------------*/
    function MobileMenu() {

      // Mobile Menu Open
      $("#menu-btn").on("click", function() {
        $(this).toggleClass("is-active");
      });

      $("#menu-modal").iziModal({
        fullscreen: true,
        appendTo: '#masthead', // or false
        overlay: false,
        transitionIn: "fadeIn",
        transitionOut: "fadeOut",
        onOpening: function() {
          $("body").addClass("menu-modal-open");
        },
        onClosing: function() {
          $("body").removeClass("menu-modal-open");
        }
      });
      $(document).on("click", "#menu-btn", function(e) {
        e.preventDefault(), $("#menu-modal").iziModal("open");
      });
    }


      /*--------------------------------------------------------------
      Accordions
      --------------------------------------------------------------*/
      function Accordions() {
      $(document).ready(function() {
        checkWidth();
      });
      $(window).on("resize", function(){
          checkWidth();
      });

      function checkWidth(){
        $vWidth = $(window).width();
        //Check condition for screen width
        if($vWidth < 900){
              $("li.menu-parent").on("click", function() {
                  if ($(this).hasClass("active")) {
                    $(this).removeClass("active");
                    $(this)
                      .children(".submenu-container")
                      .slideUp(200);
                  } else {
                    $("li.menu-parent").removeClass("active");
                    $(this).addClass("active");
                    $(".submenu-container").slideUp(200);
                    $(this)
                      .children(".submenu-container")
                      .slideDown(200);
                  }
                });
            }
        }

        $("li.menu-parent-primary").on("click", function() {
            if ($(this).hasClass("active")) {
              $(this).removeClass("active");
              $(this)
                .children(".submenu-container")
                .slideUp(200);
            } else {
              $("li.menu-parent-primary").removeClass("active");
              $(this).addClass("active");
              $("li.menu-parent-primary .submenu-container").slideUp(200);
              $(this)
                .children(".submenu-container")
                .slideDown(200);
            }
          });

          $(".accordion").on("click", function() {
              if ($(this).hasClass("active")) {
                $(this).removeClass("active");
                $(this)
                  .children(".accordion-container")
                  .slideUp(200);
              } else {
                $(".accordion").removeClass("active");
                $(this).addClass("active");
                $(".accordion .accordion-container").slideUp(200);
                $(this)
                  .children(".accordion-container")
                  .slideDown(200);
              }
            });
      }


    SmoothScroll();
    MobileMenu();
    Modals();
    Accordions();

  });
})(jQuery);
