<?php
/**
 * The front page template file
 *
 */

get_header(); ?>

<div id="primary" class="content-area"><!-- #primary -->
	<main id="main" class="site-main" role="main"><!-- #main -->


        <?php get_template_part('template-parts/page/content-front-page'); ?>


	</main><!-- #main -->
</div><!-- #primary -->

<?php get_footer();
