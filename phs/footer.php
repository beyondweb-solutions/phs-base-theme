<?php
/**
 * The template for displaying the footer
 *
 */

?>

		</div><!-- #content -->
</div><!-- #page -->


		<footer id="colophon" class="site-footer" role="contentinfo">


                <div class="footer-row upper">
									<div class="wrap">
											<div class="footer-row-container">
												<?php
														wp_nav_menu( array(
																'theme_location' => 'footer',
																'menu_id'        => 'footer-menu',
																'walker' => new Walker_Nav_Main()
														) );
												?>
												<div class="footer-social">
														<?php if( have_rows('footer_social_icons', 'option') ): ?>
																<div class="social-logos">
																		<?php while( have_rows('footer_social_icons', 'option') ): the_row(); ?>
																				<a href="<?php the_sub_field('menu_social_url') ?>" target="_blank">
																						<img class="svg" src="<?php the_sub_field('social_media_icon') ?>">
																				</a>
																		<?php endwhile; ?>
																</div>
														<?php endif; ?>
												</div>
										  </div>
									 </div>
                </div>

                <div class="footer-row bottom">
										<div class="wrap">
											<div class="footer-row-container">
												<div class="footer-col legal">
		                        <p><?php the_field('footer_legal_text', 'option') ?></p>
		                    </div>

												<div class="footer-col terms">
		                        <a href="<?php the_field('footer_terms_link', 'option') ?>" target="_blank"><?php the_field('footer_terms_text', 'option') ?></a>
														<a href="<?php the_field('footer_privacy_link', 'option') ?>" target="_blank"><?php the_field('footer_privacy_text', 'option') ?></a>
		                    </div>

		                    <div class="footer-col website">
		                        <p>Built By<a href="https://beyondweb.solutions/" target="_blank"> Beyondweb.solutions</a></p>
		                    </div>
											</div>
									  </div>
                </div>




		</footer><!-- #colophon -->


<!-- Menu Modal -->
<div id="menu-modal" style="background: <?php the_field('menu_bg_color', 'option') ?> url('<?php the_field('menu_bg_image', 'option') ?>')">
		<nav>
			<?php
					wp_nav_menu( array(
							'theme_location' => 'main',
							'menu_id'        => 'main-menu',
							'walker' => new Walker_Nav_Main()
					) );
			?>
		</nav>
		<div id="menu-nav-extra">
			<div class="wrap">

			<div class="menu-social">
					<?php if( have_rows('menu_social_icons', 'option') ): ?>
							<div class="social-logos">
									<?php while( have_rows('menu_social_icons', 'option') ): the_row(); ?>
											<a href="<?php the_sub_field('menu_social_url', 'option') ?>" target="_blank">
													<img class="svg" src="<?php the_sub_field('menu_social_media_icon', 'option') ?>">
											</a>
									<?php endwhile; ?>
							</div>
					<?php endif; ?>
			</div>

				<?php if( have_rows('menu_contact_options', 'option') ): ?>
					<div class="menu-contact">
								<?php while( have_rows('menu_contact_options', 'option') ): the_row(); ?>
										<a href="<?php the_sub_field('menu_contact_url', 'option') ?>" target="_blank"><?php the_sub_field('menu_contact_text', 'option') ?></a>
								<?php endwhile; ?>
					</div>
				<?php endif; ?>

			</div>
		</div>
</div>


<?php wp_footer(); ?>

</body>
</html>
