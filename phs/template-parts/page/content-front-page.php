<?php
/**
 * Template part for displaying page content in page.php
 *
 */

?>


<?php if ( have_rows( 'content_layouts') ):
    while ( have_rows( 'content_layouts') ):the_row();

    $layout = get_row_layout();

    switch($layout) {
        case 'layout_front_hero':
            get_template_part('template-parts/layout/frontpage-hero');
            break;
        case 'layout_front_intro':
            get_template_part('template-parts/layout/frontpage-intro-block');
            break;
        case 'layout_front_ourschool':
            get_template_part('template-parts/layout/frontpage-ourschool-block');
            break;
        case 'layout_front_life':
            get_template_part('template-parts/layout/frontpage-life-block');
            break;
        case 'layout_contact_block':
            get_template_part('template-parts/layout/frontpage-contact-block');
            break;
    }


    endwhile; endif; ?>
