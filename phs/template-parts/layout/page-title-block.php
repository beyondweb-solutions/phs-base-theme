<?php
/**
 * Template part for displaying Page Title layout block
 *
 */

$bg_colour = get_sub_field( 'bg_color' );
$bg_image = get_sub_field( 'bg_image' );
$cont_width = get_sub_field( 'cont_width' );
$cont_padd = get_sub_field( 'container_padding' );
$cont_align = get_sub_field( 'text_align' );
$row_id = get_sub_field( 'row_id' );
$cont_class = get_sub_field( 'cont_class' );
$page_title = get_sub_field( 'page_title' );
$page_subtitle = get_sub_field( 'page_subtitle' );
$page_description = get_sub_field( 'page_description' );
$title_color = get_sub_field( 'title_color' );

$pattern_bg = get_sub_field( 'pattern_bg' );
$pattern_type = get_sub_field( 'pattern_type' );
$pattern_one_img = get_sub_field( 'pattern_one_img' );
$pattern_two_img = get_sub_field( 'pattern_two_img' );
$pattern_one_align = get_sub_field( 'pattern_one_align' );
$pattern_two_align = get_sub_field( 'pattern_two_align' );


?>


<section id="<?php echo $row_id ?>" class="layout-block title-block <?php echo $bg_colour ?> <?php echo $cont_padd ?> <?php echo $cont_class ?> <?php echo $cont_align ?>" style="background: url('<?php echo $bg_image ?>')">

    <div class="wrap <?php echo $cont_width ?>">

        <div class="block-content content-scroll">

          <h1 class="page-title <?php echo $title_color?>"><?php echo $page_title ?></h1>

          <?php if ($page_subtitle) { ?>
                <h4 class="subtitle"><?php echo $page_subtitle ?></h4>
          <?php } ?>
          <?php if ($page_description) { ?>
                <p><?php echo $page_description ?></p>
          <?php } ?>


          <?php if( have_rows('button') ): ?>
              <?php while( have_rows('button') ): the_row();
              $enable_button = get_sub_field( 'enable_button' );
              $button_type = get_sub_field( 'button_type' );
              $button_text = get_sub_field( 'button_text' );
              $button_colour = get_sub_field( 'button_color' );
              $button_link = get_sub_field( 'button_link' );
              $button_page = get_sub_field( 'button_page' );
              $button_url = get_sub_field( 'button_url' );
              $button_modal = get_sub_field( 'button_modal' );
                  ?>
          <?php if ( $enable_button == 'true' ) { ?>

              <?php if ( $button_type == 'internal' ) { ?>

                  <a class="btn <?php echo $button_colour; ?>"  href=" <?php echo $button_link ?>">
                      <?php if ( !empty($button_text) )  { ?>
                          <?php echo $button_text ?>
                      <?php } ?>
                  </a>

              <?php } elseif ( $button_type == 'page' ) { ?>

                  <a class="btn <?php echo $button_colour ?>"  href=" <?php echo $button_page ?>">
                      <?php if ( $button_text )  { ?>
                          <?php echo $button_text ?>
                      <?php } ?>
                  </a>

              <?php } elseif ( $button_type == 'external' ) { ?>

                  <a class="btn <?php echo $button_colour ?>"  href=" <?php echo $button_url ?>" target="_blank">
                      <?php if ( $button_text )  { ?>
                          <?php echo $button_text ?>
                      <?php } ?>
                  </a>

              <!-- Modal Button  -->
              <?php } elseif ( $button_type == 'modal' ) { ?>

                  <!-- Contact Modal  -->
                  <?php if ( $button_modal == 'contact' ) { ?>

                  <a class="btn <?php echo $button_colour ?>"  data-izimodal-open="#register-modal">
                      <?php if ( $button_text )  { ?>
                          <?php echo $button_text ?>
                      <?php } ?>
                  </a>

                  <!-- Video Modal  -->
                  <?php } elseif ( $button_modal == 'video' ) { ?>

                  <a class="btn <?php echo $button_colour ?>"  data-izimodal-open="#video-modal">
                      <?php if ( $button_text )  { ?>
                          <?php echo $button_text ?>
                      <?php } ?>
                  </a>

                  <!-- Location Modal  -->
                  <?php } elseif ( $button_modal == 'location' ) { ?>

                      <a class="btn <?php echo $button_colour ?>"  data-izimodal-open="#location-modal">
                          <?php if ( $button_text )  { ?>
                              <?php echo $button_text ?>
                          <?php } ?>
                      </a>

                  <?php } ?>


              <?php } ?>

          <?php } ?>
        <?php endwhile; ?>
    <?php endif; ?>
        </div>

    </div>

    <?php if ( $pattern_bg == 'true' ) { ?>
        <?php if ( $pattern_type == 'one' ) { ?>
          <div class="pattern-bg pattern-one" style="background:url('<?php echo $pattern_one_img ?>') <?php echo $pattern_one_align ?> no-repeat">
          </div>
        <?php } elseif ( $pattern_type == 'two' ) { ?>
          <div class="pattern-bg pattern-one" style="background:url('<?php echo $pattern_one_img ?>') <?php echo $pattern_one_align ?> no-repeat">
          </div>
          <div class="pattern-bg pattern-two" style="background:url('<?php echo $pattern_two_img ?>') <?php echo $pattern_two_align ?> no-repeat">
          </div>
        <?php } ?>
    <?php } ?>

</section>
