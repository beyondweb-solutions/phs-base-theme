<?php
/**
 * Template part for displaying Frontpage Intro layout block
 *
 */

$bg_colour = get_sub_field( 'bg_color' );
$bg_image = get_sub_field( 'bg_image' );
$cont_width = get_sub_field( 'cont_width' );
$cont_padd = get_sub_field( 'container_padding' );
$row_id = get_sub_field( 'row_id' );
$cont_class = get_sub_field( 'cont_class' );
$intro_title = get_sub_field( 'intro_title' );
$title_color = get_sub_field( 'title_color' );
?>


<section id="<?php echo $row_id ?>" class="layout-block frontpage-intro-block <?php echo $bg_colour ?> <?php echo $cont_padd ?> <?php echo $cont_class ?>" style="background: url('<?php echo $bg_image ?>')">

    <div class="wrap <?php echo $cont_width ?>">

        <div class="intro-title content-scroll">
          <h1 class="<?php echo $title_color?>"><?php echo $intro_title ?></h1>
        </div>

        <div class="intro-slider content-scroll">

            <?php if( have_rows('intro_slider') ): ?>
                <?php while( have_rows('intro_slider') ): the_row();
                    $headline = get_sub_field( 'title' );
                    $description = get_sub_field( 'description' );
                    $button_type = get_sub_field( 'button_type' );
                    $button_text = get_sub_field( 'button_text' );
                    $button_colour = get_sub_field( 'button_color' );
                    $button_link = get_sub_field( 'button_link' );
                    $button_page = get_sub_field( 'button_page' );
                    $button_url = get_sub_field( 'button_url' );
                    $button_modal = get_sub_field( 'button_modal' );
                    slick_enqueue_scripts_styles();
                    ?>

                    <div class="intro-slide">

                      <div class="tile-content">

                        <h3 class="title"><?php echo $headline ?></h3>
                        <p class="desc"><?php echo $description ?></p>

                                <?php if ( $button_type == 'internal' ) { ?>

                                    <a class="btn <?php echo $button_colour ?>"  href=" <?php echo $button_link ?>">
                                        <?php if ( $button_text )  { ?>
                                            <?php echo $button_text ?>
                                        <?php } ?>
                                    </a>

                              <?php } elseif ( $button_type == 'page' ) { ?>

                                      <a class="btn <?php echo $button_colour ?>"  href=" <?php echo $button_page ?>">
                                          <?php if ( $button_text )  { ?>
                                              <?php echo $button_text ?>
                                          <?php } ?>
                                      </a>


                                <?php } elseif ( $button_type == 'external' ) { ?>

                                    <a class="btn <?php echo $button_colour ?>"  href=" <?php echo $button_url ?>" target="_blank">
                                        <?php if ( $button_text )  { ?>
                                            <?php echo $button_text ?>
                                        <?php } ?>
                                    </a>


                                    <!-- Modal Button  -->
                                <?php } elseif ( $button_type == 'modal' ) { ?>

                                    <!-- Contact Modal  -->
                                    <?php if ( $button_modal == 'contact' ) { ?>

                                        <a class="btn <?php echo $button_colour ?>" data-izimodal-open="#register-modal">
                                            <?php if ( $button_text )  { ?>
                                                <?php echo $button_text ?>
                                            <?php } ?>
                                        </a>

                                        <!-- Video Modal  -->
                                    <?php } elseif ( $button_modal == 'video' ) { ?>

                                        <a class="btn <?php echo $button_colour ?>" data-izimodal-open="#video-modal">
                                            <?php if ( $button_text )  { ?>
                                                <?php echo $button_text ?>
                                            <?php } ?>
                                        </a>

                                        <!-- Location Modal  -->
                                    <?php } elseif ( $button_modal == 'location' ) { ?>

                                        <a class="btn <?php echo $button_colour ?>" data-izimodal-open="#location-modal">
                                            <?php if ( $button_text )  { ?>
                                                <?php echo $button_text ?>
                                            <?php } ?>
                                        </a>

                                    <?php } ?>


                                <?php } ?>
                        </div>

                    </div>

                <?php endwhile; ?>
            <?php endif; ?>

        </div>

    <a href="#our-school" class="intro-arrow">
    </div>



    </a>



</section>
