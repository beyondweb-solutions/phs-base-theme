<?php
/**
 * Template part for displaying Contact Block layout block
 *
 */

$bg_colour = get_sub_field( 'bg_color' );
$bg_image = get_sub_field( 'bg_image' );
$cont_width = get_sub_field( 'cont_width' );
$cont_padd = get_sub_field( 'container_padding' );
$cont_align = get_sub_field( 'text_align' );
$row_id = get_sub_field( 'row_id' );
$cont_class = get_sub_field( 'cont_class' );
$contact_title = get_sub_field( 'contact_title' );
$title_color = get_sub_field( 'title_color' );

$pattern_bg = get_sub_field( 'pattern_bg' );
$pattern_type = get_sub_field( 'pattern_type' );
$pattern_one_img = get_sub_field( 'pattern_one_img' );
$pattern_two_img = get_sub_field( 'pattern_two_img' );
$pattern_one_align = get_sub_field( 'pattern_one_align' );
$pattern_two_align = get_sub_field( 'pattern_two_align' );

?>


<section id="<?php echo $row_id ?>" class="layout-block contact-block <?php echo $bg_colour ?> <?php echo $cont_padd ?> <?php echo $cont_class ?> <?php echo $cont_align ?>" style="background: url('<?php echo $bg_image ?>')">

    <div class="wrap <?php echo $cont_width ?>">

        <div class="contact-title content-scroll">
          <h2 class="front-title <?php echo $title_color?>"><?php echo $contact_title ?></h2>
        </div>

        <div class="contact-slider content-scroll">

            <?php if( have_rows('contact_slider') ): ?>
                <?php while( have_rows('contact_slider') ): the_row();
                    $contact_icon = get_sub_field( 'icon' );
                    $contact_title = get_sub_field( 'title' );
                    $contact_desc = get_sub_field( 'description' );
                    $button_type = get_sub_field( 'button_type' );
                    $button_text = get_sub_field( 'button_text' );
                    $button_colour = get_sub_field( 'button_color' );
                    $button_link = get_sub_field( 'button_link' );
                    $button_page = get_sub_field( 'button_page' );
                    $button_url = get_sub_field( 'button_url' );
                    $button_modal = get_sub_field( 'button_modal' );
                    slick_enqueue_scripts_styles();
                    ?>

                    <div class="contact-icon">

                      <?php if ( $button_type == 'internal' ) { ?>

                            <a class=""  href="<?php echo $button_link ?>">
                              <img class="svg" src="<?php echo $contact_icon ?>">
                              <p class="headline"><?php echo $contact_title ?></p>
                              <p><?php echo $contact_desc ?></p>
                            </a>

                      <?php } elseif ( $button_type == 'page' ) { ?>

                        <a class=""  href="<?php echo $button_page ?>">
                          <img class="svg" src="<?php echo $contact_icon ?>">
                          <p class="headline"><?php echo $contact_title ?></p>
                          <p><?php echo $contact_desc ?></p>
                        </a>


                        <?php } elseif ( $button_type == 'external' ) { ?>


                            <a class=""  href="<?php echo $button_url ?>" target="_blank">
                              <img class="svg" src="<?php echo $contact_icon ?>">
                              <p class="headline"><?php echo $contact_title ?></p>
                              <p><?php echo $contact_desc ?></p>
                            </a>


                            <!-- Modal Button  -->
                        <?php } elseif ( $button_type == 'modal' ) { ?>

                            <!-- Contact Modal  -->
                            <?php if ( $button_modal == 'contact' ) { ?>

                                <a class="" data-izimodal-open="#register-modal">
                                  <img class="svg" src="<?php echo $contact_icon ?>">
                                  <p class="headline"><?php echo $contact_title ?></p>
                                  <p><?php echo $contact_desc ?></p>
                                </a>

                                <!-- Video Modal  -->
                            <?php } elseif ( $button_modal == 'video' ) { ?>

                                <a class="" data-izimodal-open="#video-modal">
                                  <img class="svg" src="<?php echo $contact_icon ?>">
                                  <p class="headline"><?php echo $contact_title ?></p>
                                  <p><?php echo $contact_desc ?></p>
                                </a>

                                <!-- Location Modal  -->
                            <?php } elseif ( $button_modal == 'location' ) { ?>

                                <a class="" data-izimodal-open="#location-modal">
                                  <img class="svg" src="<?php echo $contact_icon ?>">
                                  <p class="headline"><?php echo $contact_title ?></p>
                                  <p><?php echo $contact_desc ?></p>
                                </a>

                            <?php } ?>


                        <?php } ?>
                    </div>

                <?php endwhile; ?>
            <?php endif; ?>

        </div>


    </div>

    <?php if ( $pattern_bg == 'true' ) { ?>
        <?php if ( $pattern_type == 'one' ) { ?>
          <div class="pattern-bg pattern-one" style="background:url('<?php echo $pattern_one_img ?>') <?php echo $pattern_one_align ?> no-repeat">
          </div>
        <?php } elseif ( $pattern_type == 'two' ) { ?>
          <div class="pattern-bg pattern-one" style="background:url('<?php echo $pattern_one_img ?>') <?php echo $pattern_one_align ?> no-repeat">
          </div>
          <div class="pattern-bg pattern-two" style="background:url('<?php echo $pattern_two_img ?>') <?php echo $pattern_two_align ?> no-repeat">
          </div>
        <?php } ?>
    <?php } ?>

</section>
