<?php
/**
 * Template part for displaying Image layout block
 *
 */

 $bg_colour = get_sub_field( 'bg_color' );
 $bg_image = get_sub_field( 'bg_image' );
 $cont_width = get_sub_field( 'cont_width' );
 $cont_padd = get_sub_field( 'container_padding' );
 $row_id = get_sub_field( 'row_id' );
 $cont_class = get_sub_field( 'cont_class' );

 $pattern_bg = get_sub_field( 'pattern_bg' );
 $pattern_type = get_sub_field( 'pattern_type' );
 $pattern_one_img = get_sub_field( 'pattern_one_img' );
 $pattern_two_img = get_sub_field( 'pattern_two_img' );
 $pattern_one_align = get_sub_field( 'pattern_one_align' );
 $pattern_two_align = get_sub_field( 'pattern_two_align' );


?>


<section id="<?php echo $row_id ?>" class="layout-block image-block <?php echo $bg_colour ?> <?php echo $cont_padd ?> <?php echo $cont_class ?> " style="background: url('<?php echo $bg_image ?>')">

    <div class="wrap <?php echo $cont_width ?>">

        <div class="main-image image-scroll">

          <?php if( have_rows('images') ): ?>
                <?php while( have_rows('images') ): the_row();
                $single_img = get_sub_field( 'single_image' );
                $bg_size = get_sub_field( 'bg_size' );
                $bg_position = get_sub_field( 'bg_position' );
                    ?>

                  <div class="phs-image-main <?php echo $bg_size ?> <?php echo $bg_position ?>" style="background: url('<?php echo $single_img ?>')">
                  </div>

              <?php endwhile; ?>
          <?php endif; ?>

        </div>

    </div>

    </div>

    <?php if ( $pattern_bg == 'true' ) { ?>
        <?php if ( $pattern_type == 'one' ) { ?>
          <div class="pattern-bg pattern-one" style="background:url('<?php echo $pattern_one_img ?>') <?php echo $pattern_one_align ?> no-repeat">
          </div>
        <?php } elseif ( $pattern_type == 'two' ) { ?>
          <div class="pattern-bg pattern-one" style="background:url('<?php echo $pattern_one_img ?>') <?php echo $pattern_one_align ?> no-repeat">
          </div>
          <div class="pattern-bg pattern-two" style="background:url('<?php echo $pattern_two_img ?>') <?php echo $pattern_two_align ?> no-repeat">
          </div>
        <?php } ?>
    <?php } ?>


</section>
