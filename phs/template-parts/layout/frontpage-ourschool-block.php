<?php
/**
 * Template part for displaying Frontpage Our School layout block
 *
 */
 $ourschool_title = get_sub_field( 'ourschool_title' );
 $title_color = get_sub_field( 'title_color' );
 $row_id = get_sub_field( 'row_id' );
 $cont_class = get_sub_field( 'cont_class' );
?>


<section id="<?php echo $row_id ?>" class="layout-block frontpage-ourschool-block <?php echo $cont_class ?>">

      <div class="ourschool-wrap">

        <div class="ourschool-main">

          <div class="ourschool-mainslider">

            <?php if( have_rows('ourschool_mainslider') ): $list_count = 0; ?>
                <?php while( have_rows('ourschool_mainslider') ): the_row();
                    $main_img = get_sub_field( 'background_image' );
                    $headline = get_sub_field( 'title' );
                    $short_headline = get_sub_field( 'page_title' );
                    $description = get_sub_field( 'description' );
                    $button_type = get_sub_field( 'button_type' );
                    $button_text = get_sub_field( 'button_text' );
                    $button_colour = get_sub_field( 'button_color' );
                    $button_link = get_sub_field( 'button_link' );
                    $button_page = get_sub_field( 'button_page' );
                    $button_url = get_sub_field( 'button_url' );
                    $button_modal = get_sub_field( 'button_modal' );
                    ?>
                    <div class="mainslide-container" style="background: url('<?php echo $main_img ?>')">
                      <div class="ourschool-mainslide">
                        <div class="ourschool-mainslide-content">

                          <h2 class="front-title"><?php echo $short_headline ?></h2>
                          <h3 class="title"><?php echo $headline ?></h3>
                          <p class="desc"><?php echo $description ?></p>

                            <?php if ( $button_type == 'internal' ) { ?>

                                      <a class="btn <?php echo $button_colour ?>"  href=" <?php echo $button_link ?>">
                                          <?php if ( $button_text )  { ?>
                                              <?php echo $button_text ?>
                                          <?php } ?>
                                      </a>

                                <?php } elseif ( $button_type == 'page' ) { ?>

                                        <a class="btn <?php echo $button_colour ?>"  href=" <?php echo $button_page ?>">
                                            <?php if ( $button_text )  { ?>
                                                <?php echo $button_text ?>
                                            <?php } ?>
                                        </a>


                                  <?php } elseif ( $button_type == 'external' ) { ?>

                                      <a class="btn <?php echo $button_colour ?>"  href=" <?php echo $button_url ?>" target="_blank">
                                          <?php if ( $button_text )  { ?>
                                              <?php echo $button_text ?>
                                          <?php } ?>
                                      </a>


                                      <!-- Modal Button  -->
                                  <?php } elseif ( $button_type == 'modal' ) { ?>

                                      <!-- Contact Modal  -->
                                      <?php if ( $button_modal == 'contact' ) { ?>

                                          <a class="btn <?php echo $button_colour ?>" data-izimodal-open="#register-modal">
                                              <?php if ( $button_text )  { ?>
                                                  <?php echo $button_text ?>
                                              <?php } ?>
                                          </a>

                                          <!-- Video Modal  -->
                                      <?php } elseif ( $button_modal == 'video' ) { ?>

                                          <a class="btn <?php echo $button_colour ?>" data-izimodal-open="#video-modal">
                                              <?php if ( $button_text )  { ?>
                                                  <?php echo $button_text ?>
                                              <?php } ?>
                                          </a>

                                          <!-- Location Modal  -->
                                      <?php } elseif ( $button_modal == 'location' ) { ?>

                                          <a class="btn <?php echo $button_colour ?>" data-izimodal-open="#location-modal">
                                              <?php if ( $button_text )  { ?>
                                                  <?php echo $button_text ?>
                                              <?php } ?>
                                          </a>

                                      <?php } ?>


                                  <?php } ?>
                          </div>
                      </div>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
          </div>

        </div>


        <div class="ourschool-secondary content-scroll">
          <div class="ourschool-title">
            <h2 class="front-title <?php echo $title_color?>"><?php echo $ourschool_title ?></h2>
          </div>
          <div class="ourschool-secondaryslider">
            <?php if( have_rows('ourschool_mainslider') ): $list_count = 0; ?>
                <?php while( have_rows('ourschool_mainslider') ): the_row();
                    $icon = get_sub_field( 'icon' );
                    $short_headline = get_sub_field( 'page_title' );
                    $short_description = get_sub_field( 'short_description' );
                    $list_count++;
                    ?>

                    <div class="ourschool-secondaryslide-content">

                          <a class="secondaryslide-container" data-slide="<?php echo $list_count ?>">
                            <div class="content-left">
                              <img class="svg" src="<?php echo $icon ?>">
                            </div>
                            <div class="content-right">
                                <h4 class="title"><?php echo $short_headline ?></h4>
                                <p class="desc"><?php echo $short_description ?></p>
                            </div>
                          </a>

                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
          </div>

        </div>

    </div>

    </div>

</section>
