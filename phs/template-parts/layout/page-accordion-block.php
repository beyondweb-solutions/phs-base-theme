<?php
/**
 * Template part for displaying Page Accordion layout block
 *
 */

$bg_colour = get_sub_field( 'bg_color' );
$bg_image = get_sub_field( 'bg_image' );
$cont_width = get_sub_field( 'cont_width' );
$cont_padd = get_sub_field( 'container_padding' );
$cont_align = get_sub_field( 'text_align' );
$row_id = get_sub_field( 'row_id' );
$cont_class = get_sub_field( 'cont_class' );

$pattern_bg = get_sub_field( 'pattern_bg' );
$pattern_type = get_sub_field( 'pattern_type' );
$pattern_one_img = get_sub_field( 'pattern_one_img' );
$pattern_two_img = get_sub_field( 'pattern_two_img' );
$pattern_one_align = get_sub_field( 'pattern_one_align' );
$pattern_two_align = get_sub_field( 'pattern_two_align' );

$table = get_sub_field( 'table' );
?>


<section id="<?php echo $row_id ?>" class="layout-block accordion-block <?php echo $bg_colour ?> <?php echo $cont_padd ?> <?php echo $cont_class ?> <?php echo $cont_align ?>" style="background: url('<?php echo $bg_image ?>')">

    <div class="wrap <?php echo $cont_width ?>">

  <?php if( have_rows('accordion') ): ?>
        <?php while( have_rows('accordion') ): the_row();
        $accordion_title = get_sub_field( 'accordion_title' );
        $accordion_style = get_sub_field( 'accordion_style' );
        $content_type = get_sub_field( 'content_type' );
            ?>

      <div class="accordion content-scroll <?php echo $accordion_style ?>">
        <p class="acc-title"><?php echo $accordion_title ?></p>

        <div class="accordion-container">

              <?php if ( $content_type == 'text' ) { ?>

                  <div class="accordion-content">
                    <?php if( have_rows('text') ): ?>
                      <?php while( have_rows('text') ): the_row();
                      $page_subtitle = get_sub_field( 'page_subtitle' );
                      $page_text = get_sub_field( 'page_text' );
                          ?>
                      <?php if ($page_subtitle) { ?>
                            <h4 class="subtitle"><?php echo $page_subtitle ?></h4>
                      <?php } ?>
                      <?php if ($page_text) { ?>
                            <?php echo $page_text ?>
                      <?php } ?>
                      <?php endwhile; ?>
                  <?php endif; ?>
                </div>

              <?php } elseif ( $content_type == 'table' ) { ?>

                <div class="accordion-table">
                    <?php if ( ! empty ( $table ) ) {

                        echo '<table border="0">';

                            if ( ! empty( $table['caption'] ) ) {

                                echo '<caption>' . $table['caption'] . '</caption>';
                            }

                            if ( ! empty( $table['header'] ) ) {

                                echo '<thead>';

                                    echo '<tr>';

                                        foreach ( $table['header'] as $th ) {

                                            echo '<th>';
                                                echo $th['c'];
                                            echo '</th>';
                                        }

                                    echo '</tr>';

                                echo '</thead>';
                            }

                            echo '<tbody>';

                                foreach ( $table['body'] as $tr ) {

                                    echo '<tr>';

                                        foreach ( $tr as $td ) {

                                            echo '<td>';
                                                echo $td['c'];
                                            echo '</td>';
                                        }

                                    echo '</tr>';
                                }

                            echo '</tbody>';

                        echo '</table>';
                    } ?>
                </div>

              <?php } elseif ( $content_type == 'text-table' ) { ?>
                    <div class="accordion-content">
                      <?php if( have_rows('text') ): ?>
                        <?php while( have_rows('text') ): the_row();
                        $page_subtitle = get_sub_field( 'page_subtitle' );
                        $page_text = get_sub_field( 'page_text' );
                            ?>
                        <?php if ($page_subtitle) { ?>
                              <h4 class="subtitle"><?php echo $page_subtitle ?></h4>
                        <?php } ?>
                        <?php if ($page_text) { ?>
                              <?php echo $page_text ?>
                        <?php } ?>
                        <?php endwhile; ?>
                    <?php endif; ?>
                  </div>
                  <div class="accordion-table">
                      <?php if ( ! empty ( $table ) ) {

                          echo '<table border="0">';

                              if ( ! empty( $table['caption'] ) ) {

                                  echo '<caption>' . $table['caption'] . '</caption>';
                              }

                              if ( ! empty( $table['header'] ) ) {

                                  echo '<thead>';

                                      echo '<tr>';

                                          foreach ( $table['header'] as $th ) {

                                              echo '<th>';
                                                  echo $th['c'];
                                              echo '</th>';
                                          }

                                      echo '</tr>';

                                  echo '</thead>';
                              }

                              echo '<tbody>';

                                  foreach ( $table['body'] as $tr ) {

                                      echo '<tr>';

                                          foreach ( $tr as $td ) {

                                              echo '<td>';
                                                  echo $td['c'];
                                              echo '</td>';
                                          }

                                      echo '</tr>';
                                  }

                              echo '</tbody>';

                          echo '</table>';
                      } ?>
                  </div>
                  <div class="accordion-content">
                    <?php if( have_rows('text') ): ?>
                          <?php while( have_rows('text') ): the_row();
                          $page_text_below = get_sub_field( 'text_below' );
                              ?>
                          <?php if ($page_text_below) { ?>
                                <?php echo $page_text_below ?>
                          <?php } ?>
                        <?php endwhile; ?>
                    <?php endif; ?>
                  </div>

                <?php } ?>
              </div>
      </div>
    <?php endwhile; ?>
<?php endif; ?>

    </div>

    <?php if ( $pattern_bg == 'true' ) { ?>
        <?php if ( $pattern_type == 'one' ) { ?>
          <div class="pattern-bg pattern-one" style="background:url('<?php echo $pattern_one_img ?>') <?php echo $pattern_one_align ?> no-repeat">
          </div>
        <?php } elseif ( $pattern_type == 'two' ) { ?>
          <div class="pattern-bg pattern-one" style="background:url('<?php echo $pattern_one_img ?>') <?php echo $pattern_one_align ?> no-repeat">
          </div>
          <div class="pattern-bg pattern-two" style="background:url('<?php echo $pattern_two_img ?>') <?php echo $pattern_two_align ?> no-repeat">
          </div>
        <?php } ?>
    <?php } ?>

</section>
