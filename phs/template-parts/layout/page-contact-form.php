<?php
/**
 * Template part for displaying Contact Form
 *
 */


$row_id = get_sub_field( 'row_id' );
$cont_class = get_sub_field( 'cont_class' );
$bg_colour = get_sub_field( 'bg_color' );
$img_desktop = get_sub_field( 'hero_image_desktop' );
$img_mobile = get_sub_field( 'hero_image_mobile' );
$form_code = get_sub_field( 'form_code' );

?>


  <section id="<?php echo $row_id ?>" class="page-contact-hero <?php echo $bg_colour ?>">
      <div class="contact-hero-container">
        <div class="bg-image">
          <div class="bg-image-main bg-image-desktop" style="background:url('<?php echo $img_desktop ?>') no-repeat center center;" /></div>
          <div class="bg-image-main bg-image-mobile" style="background:url('<?php echo $img_mobile ?>') no-repeat center center;" /></div>
        </div>


            <div class="wrap medium">
              <div class="contact-hero-content">
                <div class="contact-info content-scroll">
                  <?php if( have_rows('text') ): ?>
                      <?php while( have_rows('text') ): the_row();
                        $page_title = get_sub_field( 'page_title' );
                        $page_subtitle = get_sub_field( 'page_subtitle' );
                        $page_description = get_sub_field( 'page_description' );
                        $title_color = get_sub_field( 'title_color' );
                            ?>

                      <h2 class="page-title <?php echo $title_color?>"><?php echo $page_title ?></h2>
                      <?php if ($page_subtitle) { ?>
                          <h4 class="subtitle"><?php echo $page_subtitle ?></h4>
                      <?php } ?>
                      <?php if ($page_description) { ?>
                            <p><?php echo $page_description ?></p>
                      <?php } ?>
                      <div class="contact-slider">
                            <?php if( have_rows('contact_slider') ): ?>
                                <?php while( have_rows('contact_slider') ): the_row();
                                    $contact_icon = get_sub_field( 'icon' );
                                    $contact_title = get_sub_field( 'title' );
                                    $contact_desc = get_sub_field( 'description' );
                                    $button_type = get_sub_field( 'button_type' );
                                    $button_text = get_sub_field( 'button_text' );
                                    $button_colour = get_sub_field( 'button_color' );
                                    $button_link = get_sub_field( 'button_link' );
                                    $button_page = get_sub_field( 'button_page' );
                                    $button_url = get_sub_field( 'button_url' );
                                    $button_modal = get_sub_field( 'button_modal' );
                                    slick_enqueue_scripts_styles();
                                    ?>

                                    <div class="contact-icon">

                                      <?php if ( $button_type == 'internal' ) { ?>

                                            <a class=""  href="<?php echo $button_link ?>">
                                              <img class="svg" src="<?php echo $contact_icon ?>">
                                              <p class="headline"><?php echo $contact_title ?></p>
                                              <p><?php echo $contact_desc ?></p>
                                            </a>

                                      <?php } elseif ( $button_type == 'page' ) { ?>

                                        <a class=""  href="<?php echo $button_page ?>">
                                          <img class="svg" src="<?php echo $contact_icon ?>">
                                          <p class="headline"><?php echo $contact_title ?></p>
                                          <p><?php echo $contact_desc ?></p>
                                        </a>


                                        <?php } elseif ( $button_type == 'external' ) { ?>


                                            <a class=""  href="<?php echo $button_url ?>" target="_blank">
                                              <img class="svg" src="<?php echo $contact_icon ?>">
                                              <p class="headline"><?php echo $contact_title ?></p>
                                              <p><?php echo $contact_desc ?></p>
                                            </a>


                                            <!-- Modal Button  -->
                                        <?php } elseif ( $button_type == 'modal' ) { ?>

                                            <!-- Contact Modal  -->
                                            <?php if ( $button_modal == 'contact' ) { ?>

                                                <a class="" data-izimodal-open="#register-modal">
                                                  <img class="svg" src="<?php echo $contact_icon ?>">
                                                  <p class="headline"><?php echo $contact_title ?></p>
                                                  <p><?php echo $contact_desc ?></p>
                                                </a>

                                                <!-- Video Modal  -->
                                            <?php } elseif ( $button_modal == 'video' ) { ?>

                                                <a class="" data-izimodal-open="#video-modal">
                                                  <img class="svg" src="<?php echo $contact_icon ?>">
                                                  <p class="headline"><?php echo $contact_title ?></p>
                                                  <p><?php echo $contact_desc ?></p>
                                                </a>

                                                <!-- Location Modal  -->
                                            <?php } elseif ( $button_modal == 'location' ) { ?>

                                                <a class="" data-izimodal-open="#location-modal">
                                                  <img class="svg" src="<?php echo $contact_icon ?>">
                                                  <p class="headline"><?php echo $contact_title ?></p>
                                                  <p><?php echo $contact_desc ?></p>
                                                </a>

                                            <?php } ?>


                                        <?php } ?>
                                    </div>

                                <?php endwhile; ?>
                            <?php endif; ?>
                          </div>
                    <?php endwhile; ?>
                    <?php endif; ?>


                </div>

                <div class="contact-form content-scroll">
                  <div class="contact-form-main">
                      <h5>Get In Touch</h5>
                      <?php echo do_shortcode("$form_code");  ?>
                  </div>
                </div>

          </div>
        </div>


      </div>
    </section>
